INCLUDE explore
INCLUDE exploration_locations
INCLUDE rest
INCLUDE rest_events
INCLUDE time_advance
INCLUDE town
INCLUDE player_status
INCLUDE camp
INCLUDE camp_events
INCLUDE clues
INCLUDE utility
INCLUDE destinations
INCLUDE exhausted
INCLUDE landscape





VAR day = 0
VAR energy = 4

-> intro

=== intro ===

Start of the game!

// meet irate person who tells you that people don't want to leave
// that there are places inland that are far to get to
// that it's unsafe so go slowly
// that a further shore is reachable from here
// that you at least seem to be free of the mania of the place

-> town_options


