INCLUDE explore
INCLUDE exploration_locations
INCLUDE rest
INCLUDE rest_events
INCLUDE time_advance
INCLUDE town
INCLUDE player_status
INCLUDE camp
INCLUDE camp_events
INCLUDE clues
INCLUDE utility
INCLUDE destinations
INCLUDE exhausted
INCLUDE landscape
INCLUDE intro


VAR day = 0
VAR energy = 4

DEBUG OPTIONS - DELETE BEFORE FINAL BUILD
+ go to intro
    -> intro
+ go to town options
    -> town_options

